package io.mountblue.ipl;

import io.mountblue.ipl.Delivery;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Main {
    final static  int MATCH_ID = 0;
    final static int INNING = 1;
    final static int BATTING_TEAM = 2;
    final static int BOWLING_TEAM= 3;
    final static int OVER = 4;
    final static int BALL = 5;
    final static int BATSMAN = 6;
    final static int  NON_STRIKER= 7;
    final static int  BOWLER= 8;
    final static int IS_SUPER_OVER = 9;
    final static int WIDE_RUNS = 10;
    final static int BYE_RUNS = 11;
    final  static int LEGBYE_RUNS = 12;
    final static int NOBALL_RUNS = 13;
    final static int PENALITY_RUNS = 14;
    final static  int BATSMAN_RUNS = 15;
    final static int EXTRA_RUNS = 16;
    final static int TOTAL_RUNS = 17;
    final static int PLAYER_DISMISSED = 18;
    final static int DISMISSAL_KIND = 19;
    final static int FIELDER = 20;
    final static int SEASON = 1;
    final static int CITY = 2;
    final static int DATE = 3;
    final static int TEAM1 = 4;
    final static int TEAM2 = 5;
    final static int TOSS_WINNER = 6;
    final static int TOSS_DECISION = 7;
    final static int RESULT = 8;
    final static int DL_APPLIED = 9;
    final static int WINNER = 10;
    final static int WIN_BY_RUNS = 11;
    final static int WIN_BY_WICKETS = 12;
    final static int PLAYER_OF_MATCH = 13;
    final static int VENUE = 14;
    final static int UMPIRE1 = 15;
    final static int UMPIRE2 = 16;
    final static int UMPIRE3 = 17;

    public static void main(String[] args) {
        List<Delivery> deliveries = getDeliveriesData();
        List<Match> matches = getMatchesData();

        findMatchesPlayedPerYearInIpl(matches);
//        findMatchesWonPerTeam(matches);
//        findExtraRunsScoredIn2016PerTeam(matches, deliveries);
//        findTopEconomicalBowlersOf2015(matches, deliveries);
//        findMostRunsScoredByAPlayerInChennaiIn2011(matches, deliveries);
//        findMostSixesScoredByEachBatsmanInATeam(deliveries, "Royal Challengers Bangalore");
//        findMostSixesScoringBatsmanOfPerTeam(deliveries);
//        findVenueWhereSunrisersHyderabadWonMaximumMatchesBattingFirst(matches);
//        findVenueWithMostSixes(deliveries, matches);
//        findVenueWhereTeamBattingFirstWonHighestNumberOfMatches(matches);

    }

    private static void findVenueWithMostSixes(List<Delivery> deliveriesData, List<Match> matchesData) {
    Map<String, Set<Integer>> listOfVenuesWithMatchIds = new HashMap<>();
    for(Match match : matchesData){
        Set<Integer> matchIds = new HashSet<>();
        if(listOfVenuesWithMatchIds.containsKey(match.getCity())){
            matchIds = listOfVenuesWithMatchIds.get(match.getCity());
        }
            matchIds.add(match.getMatchId());
        listOfVenuesWithMatchIds.put(match.getCity(), matchIds);
    }
Map<String, Integer> venuesWithTotalNumberOfSixes = new HashMap<>();
    for(Map.Entry<String, Set<Integer>> venueWithMatchIds : listOfVenuesWithMatchIds.entrySet()){
        for(Delivery delivery : deliveriesData){
            if(venueWithMatchIds.getValue().contains(delivery.getMatchId())){
                venuesWithTotalNumberOfSixes.put(venueWithMatchIds.getKey(),  venuesWithTotalNumberOfSixes.getOrDefault(venueWithMatchIds.getKey(), 0)+1);
            }
        }
    }
        for(Map.Entry m :venuesWithTotalNumberOfSixes.entrySet()){
            System.out.println(m.getKey());
        }
//        System.out.println(venuesWithTotalNumberOfSixes.get("Hyderabad"));
    }


    private static void findVenueWhereTeamBattingFirstWonHighestNumberOfMatches(List<Match> matchesData) {
        
    }

    private static void findVenueWhereSunrisersHyderabadWonMaximumMatchesBattingFirst(List<Match> matchesData) {
    Map<String, Integer> venueWithMatchesWonBySunrisersHyderabadBattingFirst = new HashMap<>();
    for(Match match : matchesData){
        if(match.getWinner().equals("Sunrisers Hyderabad") && match.getWinByRuns()!=0){
            venueWithMatchesWonBySunrisersHyderabadBattingFirst.put(match.getVenue(), venueWithMatchesWonBySunrisersHyderabadBattingFirst.getOrDefault(match.getVenue(), 0)+1);
        }
    }
    List<Map.Entry<String, Integer>> venueOrderedByWins = new ArrayList<>();
    venueOrderedByWins.addAll(venueWithMatchesWonBySunrisersHyderabadBattingFirst.entrySet());
//    Collections.sort(venueOrderedByWins, new Comparator<Map.Entry<String, Integer>>() {
//        @Override
//        public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
//
//            return o1.getValue() - o2.getValue()>0 ? -1 :1;
//        }
//    });
        Collections.sort(venueOrderedByWins, (a,b)->a.getValue()-b.getValue()>0? -1:1);
    for(Map.Entry<String, Integer> m : venueOrderedByWins){
        System.out.println((m.getKey()+" :- "+ m.getValue()));
    }
    }

    private static void findMostSixesScoringBatsmanOfPerTeam(List<String[]> deliveriesData) {
        List <String[]> teamsWithTheirMostSixesScoringBatsman = new ArrayList<>();
        Set<String> teamsInIpl = new HashSet<>();
        for(String[] delivery : deliveriesData){
            teamsInIpl.add(delivery[BATTING_TEAM]);
        }
        Iterator<String> it =teamsInIpl.iterator();
        while(it.hasNext()){
            System.out.println(it.next());
        }
//       for(String team : teamsInIpl){
//          String[] batsmanWithSixes = findMostSixesScoredByEachBatsmanInATeam(deliveriesData, team);
//           System.out.println(team+" -> "+batsmanWithSixes[0] +" :- "+ batsmanWithSixes[1]);
//       }
    }

    private static String[] findMostSixesScoredByEachBatsmanInATeam(List<Delivery> deliveries, String team) {
        Map<String, Integer> sixesPerBatsman = new HashMap<>();

        for (Delivery delivery:deliveries){
            if(delivery.getBattingTeam().equals(team) && delivery.getBatsmanRuns() == 6){
                sixesPerBatsman.put(delivery.getBatsman(), sixesPerBatsman.getOrDefault(delivery.getBatsman(), 0)+1);
            }
        }

        List<Map.Entry<String, Integer>> batsmanSortedBySixesScored = new ArrayList<>(sixesPerBatsman.entrySet());

        Collections.sort(batsmanSortedBySixesScored, (a, b )-> b.getValue()-a.getValue());

        String[] str = {batsmanSortedBySixesScored.get(0).getKey(), String.valueOf(batsmanSortedBySixesScored.get(0).getValue())};

        return  str ;
    }

    private static void findMostRunsScoredByAPlayerInChennaiIn2011(List<Match> matches, List<Delivery> deliveries) {
       Set<Integer> matchIdOfMatchesIn2016InChennai = new HashSet<>();

       for(Match match : matches){
           if(match.getSeason() ==2011 && match.getCity().equals("Chennai")){
            matchIdOfMatchesIn2016InChennai.add(match.getMatchId());
           }
       }

        Map<String, Integer> runsPerBatsmanIn2016InChennai = new HashMap<>();

        for(Delivery delivery : deliveries){
            if(!matchIdOfMatchesIn2016InChennai.contains(delivery.getMatchId())){
                continue;
            }
            try{
                runsPerBatsmanIn2016InChennai.put(delivery.getBatsman(),
                        runsPerBatsmanIn2016InChennai.getOrDefault(delivery.getBatsman(), 0)+ delivery.getBatsmanRuns());
            }
            catch (NumberFormatException e){
                continue;
        }
    }
        List<Map.Entry<String, Integer>> playersSortedbyRuns = new ArrayList<>(runsPerBatsmanIn2016InChennai.entrySet());

        Collections.sort(playersSortedbyRuns, (a,b) -> b.getValue() - a.getValue());

        for(Map.Entry<String, Integer> m : playersSortedbyRuns){
             System.out.println(m.getKey()+" :- "+ m.getValue());
        }
    }

    private static void findTopEconomicalBowlersOf2015(List<Match> matches, List<Delivery> deliveries) {
        Set<Integer> matchIdsOf2015Matches = new HashSet<>();

        for (Match match : matches){
            if(match.getSeason() == 2015){
                matchIdsOf2015Matches.add(match.getMatchId());
            }
        }

        Map<String, Integer[]> runsAndDeliveriesPerBowler = new HashMap<>();

        for(Delivery delivery:deliveries){
            if(matchIdsOf2015Matches.contains(delivery.getMatchId())){
                Integer[] runsAndDeliveries = {0,0};
                    if(delivery.getWideRuns() == 0 || delivery.getNoballRuns() == 0){
                        runsAndDeliveries[1]= 1;
                    }
                    if(delivery.getByeRuns() == 0 || delivery.getLegbyeRuns() == 0){
                        runsAndDeliveries[0] = delivery.getTotalRuns();
                    }
                if(runsAndDeliveriesPerBowler.containsKey(delivery.getBowler())){
                    runsAndDeliveries[0] += runsAndDeliveriesPerBowler.get(delivery.getBowler())[0];
                    runsAndDeliveries[1] += runsAndDeliveriesPerBowler.get(delivery.getBowler())[1];
                }
                runsAndDeliveriesPerBowler.put(delivery.getBowler(), runsAndDeliveries);
            }
        }

        Map<String, Double> economyPerBowler = new HashMap<>();

        for(Map.Entry<String, Integer[]> m : runsAndDeliveriesPerBowler.entrySet()){
            Integer[] runsAndDeliveries = m.getValue();
            economyPerBowler.put(m.getKey(), ((double) runsAndDeliveries[0])/((double) runsAndDeliveries[1])*6);
        }
        List <Map.Entry<String, Double>> bowlersSortedByTheirEconomy =new ArrayList<>(economyPerBowler.entrySet());

        Collections.sort(bowlersSortedByTheirEconomy, new Comparator<Map.Entry<String, Double>>() {
            public int compare(Map.Entry<String, Double> o1, Map.Entry<String, Double> o2) {
                if(o1.getValue() -o2.getValue() > 0) return 1;
                return  -1;
            }
        });

//        Collections.sort(bowlersSortedByTheirEconomy,(a,b)-> a.getValue().compareTo(b.getValue()));

        System.out.println(bowlersSortedByTheirEconomy.get(0).getKey()
                + " "+ bowlersSortedByTheirEconomy.get(0).getValue());
    }

    private static void findExtraRunsScoredIn2016PerTeam(List<Match> matches, List<Delivery> deliveries) {
        Set<Integer> matchIdsOf2016Matches = new HashSet<>();

        for (Match match : matches){
            if(match.getSeason() == 2016){
                   matchIdsOf2016Matches.add(match.getMatchId());
            }
        }

        Map<String, Integer> extraRunsPerTeamIn2016 = new HashMap<>();

        for (Delivery delivery : deliveries){
            if(matchIdsOf2016Matches.contains(delivery.getMatchId())){
                extraRunsPerTeamIn2016.put(delivery.getBowlingTeam(),
                        extraRunsPerTeamIn2016.getOrDefault(delivery.getBowlingTeam(), 0)+delivery.getExtraRuns());
            }
        }

        for(Map.Entry m: extraRunsPerTeamIn2016.entrySet()){
            System.out.println(m.getKey()+" :- "+m.getValue());
        }
    }

    private static void findMatchesWonPerTeam(List<Match> matches) {
        Map<String, Integer> matchesWonPerTeam =  new HashMap<>();

        for (Match match : matches){
            if(match.getWinner().equals("")){
                continue;
            }
            matchesWonPerTeam.put(match.getWinner(), matchesWonPerTeam.getOrDefault(match.getWinner(),0)+1);
        }

        for (Map.Entry m : matchesWonPerTeam.entrySet()){
            System.out.println(m.getKey()+" :- "+m.getValue());
        }
    }

    private static void findMatchesPlayedPerYearInIpl(List<Match> matches ) {
       Map<Integer, Integer> matchesPerYear =  new HashMap<>();

       for (Match match : matches){
           matchesPerYear.put(match.getSeason(), matchesPerYear.getOrDefault(match.getSeason(),0)+1);
       }

        for (Map.Entry<Integer, Integer> m: matchesPerYear.entrySet()){
            System.out.println( m.getKey()+" - "+m.getValue());
        }
    }

        private static List<Match> getMatchesData() {
           List<Match> matches = new ArrayList<>();

           try{
               FileReader file  =
                       new FileReader("/home/aditya/IdeaProjects/iplJavaProject/.idea/data/matches.csv");
               BufferedReader br = new BufferedReader(file);

               while(true){
                   String line = br.readLine();
                   if(line == null)
                   {break;}

                   String[] data = line.split(",");
                   if(data[MATCH_ID].equals("id")){
                       continue;
                   }

                   Match match = new Match();
                   match.setMatchId(Integer.parseInt(data[MATCH_ID]));
                   match.setSeason(Integer.parseInt(data[SEASON]));
                   match.setCity(data[CITY]);
                   match.setDate(data[DATE]);
                   match.setTeam1(data[TEAM1]);
                   match.setTeam2(data[TEAM2]);
                   match.setTossWinner(data[TOSS_WINNER]);
                   match.setTossDecision(data[TOSS_DECISION]);
                   match.setResult(data[RESULT]);
                   match.setDlApplied(Integer.parseInt(data[DL_APPLIED]));
                   match.setWinner(data[WINNER]);
                   match.setWinByRuns(Integer.parseInt(data[WIN_BY_RUNS]));
                   match.setWinByWickets(Integer.parseInt(data[WIN_BY_WICKETS]));
                   match.setPlayerOfMatch(data[PLAYER_OF_MATCH]);
                   match.setVenue(data[VENUE]);
                  try {
                       match.setUmpire1(data[UMPIRE1]);
                       match.setUmpire2(data[UMPIRE2]);
                       match.setUmpire3(data[UMPIRE3]);
                   }
                  catch (ArrayIndexOutOfBoundsException e){
                  }

                   matches.add(match);
               }
           }
           catch (IOException e){
               System.out.println(e);
           }
           catch (Exception e){
               System.out.println(e);
           }

    return matches;
    }

    private static List<Delivery> getDeliveriesData() {
        List<Delivery> deliveries = new ArrayList<>();

        try {
            FileReader file  =
                    new FileReader("/home/aditya/IdeaProjects/iplJavaProject/.idea/data/deliveries.csv");
            BufferedReader br = new BufferedReader(file);

            while(true){
                String line = br.readLine();
                if(line == null){
                    break;
                }

                String[] data = line.split(",");
                if(data[MATCH_ID].equals("match_id")){
                    continue;
                }

                Delivery delivery = new Delivery();
                delivery.setMatchId(Integer.parseInt(data[MATCH_ID]));
                delivery.setInning(Integer.parseInt(data[INNING]));
                delivery.setBattingTeam(data[BATTING_TEAM]);
                delivery.setBowlingTeam(data[BOWLING_TEAM]);
                delivery.setOver(Integer.parseInt(data[OVER]));
                delivery.setBall(Integer.parseInt(data[BALL]));
                delivery.setBatsman(data[BATSMAN]);
                delivery.setNonStriker(data[NON_STRIKER]);
                delivery.setBowler(data[BOWLER]);
                delivery.setIsSuperOver(Integer.parseInt(data[IS_SUPER_OVER]));
                delivery.setWideRuns(Integer.parseInt(data[WIDE_RUNS]));
                delivery.setByeRuns(Integer.parseInt(data[BYE_RUNS]));
                delivery.setLegbyeRuns(Integer.parseInt(data[LEGBYE_RUNS]));
                delivery.setNoballRuns(Integer.parseInt(data[NOBALL_RUNS]));
                delivery.setPenalityRuns(Integer.parseInt(data[PENALITY_RUNS]));
                delivery.setBatsmanRuns(Integer.parseInt(data[BATSMAN_RUNS]));
                delivery.setExtraRuns(Integer.parseInt(data[EXTRA_RUNS]));
                delivery.setTotalRuns(Integer.parseInt(data[TOTAL_RUNS]));
                try{
                    delivery.setPlayerDismissed(data[PLAYER_DISMISSED]);
                    delivery.setDismissalKind(data[DISMISSAL_KIND]);
                    delivery.setFielder(data[FIELDER]);
                }
                catch (ArrayIndexOutOfBoundsException e){
                }

                deliveries.add(delivery);
            }
        }
        catch (IOException e){
        }
        catch (Exception e){
        }

        return deliveries;
    }
}
